# Generated by Django 3.1 on 2021-01-17 12:58

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('auth', '0012_alter_user_first_name_max_length'),
    ]

    operations = [
        migrations.CreateModel(
            name='Company',
            fields=[
                ('relation', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, primary_key=True, serialize=False, to='auth.user')),
                ('tokens', models.IntegerField()),
                ('lunch', models.IntegerField()),
                ('dayoff', models.IntegerField()),
                ('day_of_month', models.IntegerField()),
                ('payoff', models.IntegerField()),
            ],
        ),
        migrations.CreateModel(
            name='Staff',
            fields=[
                ('rel', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, primary_key=True, serialize=False, to='auth.user')),
                ('tokens', models.IntegerField()),
                ('comp', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='system.company')),
            ],
        ),
    ]
