from django.urls import path
from . import views

urlpatterns = [
    path('company/login', views.companyLogin, name='companyLogin'),
    path('company/signup', views.companySignup, name='companySignup'),
    path('company/logout', views.companyLogout, name='companyLogout'),
    path('company/users', views.companyUsers, name='companyUsers'),
    path('company/dashboard', views.companyDashboard, name='companyDashboard'),
    path('company/user', views.companyUser, name='companyUser'),
    path('company/user/<str:pk>', views.companyUser, name='companyUser'),
    path('login', views.userLogin, name='userLogin'),
    path('userpage/<str:pk>', views.userPage, name='userPage'),
    path('logout', views.userLogout, name='userLogout'),

]