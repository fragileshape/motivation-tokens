from django import forms
from . import models


class CreateCompanyForm(forms.Form):
	organization = forms.CharField(max_length=30, required=True)
	email = forms.EmailField()
	password1 = forms.CharField(widget=forms.PasswordInput)
	password2 = forms.CharField(widget=forms.PasswordInput)

	def clean(self):
		cleaned_data = super(CreateCompanyForm, self).clean()
		if self.cleaned_data.get('password1') != self.cleaned_data.get('password2'):
			raise forms.ValidationError("Passwords don't match")
		elif len(self.cleaned_data.get('password1')) < 8:
			raise forms.ValidationError("Password should be at least 8 characters long")
		elif self.cleaned_data.get('password1').isdecimal():
			raise forms.ValidationError("Password can't contain only digits")
		return cleaned_data


class CreateStaffForm(forms.Form):

	username = forms.CharField(max_length=30)
	first_name = forms.CharField(max_length=30)
	last_name = forms.CharField(max_length=30)
	password1 = forms.CharField(widget=forms.PasswordInput, label='Password')
	password2 = forms.CharField(widget=forms.PasswordInput, label='Password')
	tokens = forms.IntegerField(min_value=0)

	def __init__(self, *args, **kwargs):
		choices = kwargs.pop('my_choices')
		super(CreateStaffForm, self).__init__(*args, **kwargs)
		self.fields['comp'] = forms.ChoiceField(choices=choices, widget=forms.Select(attrs={'style': 'width:180px'}))

	def clean(self):
		cleaned_data = super(CreateStaffForm, self).clean()
		if self.cleaned_data.get('password1') != self.cleaned_data.get('password2'):
			raise forms.ValidationError("Passwords don't match")
		elif len(self.cleaned_data.get('password1')) < 8:
			raise forms.ValidationError("Password should be at least 8 characters long")
		elif self.cleaned_data.get('password1').isdecimal():
			raise forms.ValidationError("Password can't contain only digits")
		return self.cleaned_data


class CompanyLogin(forms.Form):
	organization = forms.CharField(max_length=30)
	password = forms.CharField(widget=forms.PasswordInput)


class ManagePricing(forms.Form):
	lunch = forms.IntegerField(min_value=0)
	dayoff = forms.IntegerField(min_value=0)


class SchedulePayoff(forms.Form):
	day_of_month = forms.IntegerField(min_value=1, max_value=31, required=True, widget=forms.NumberInput(attrs={'style': 'width:170px'}))
	amount = forms.IntegerField(min_value=0, required=True, widget=forms.NumberInput(attrs={'style': 'width:170px'}))


class BuyTokens(forms.Form):
	tokens = forms.IntegerField(min_value=0)


class UpdateStaffForm(forms.Form):
    
    tokens = forms.IntegerField(min_value=0)


class WithdrawTokens(forms.Form):
	withdraw = forms.IntegerField(min_value=0)


class SendTokens(forms.Form):
	username = forms.CharField()
	amount = forms.IntegerField(min_value=1)


class UserLogin(forms.Form):
	username = forms.CharField(max_length=30)
	password = forms.CharField(widget=forms.PasswordInput)

