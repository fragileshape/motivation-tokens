from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from django.contrib import messages
from . import models
from . import forms

from django.contrib.auth import get_user_model

# Create your views here.

def companySignup(request):
	if request.user.is_authenticated and request.user.username in [x.relation.username for x in models.Company.objects.all()]:
		return redirect('/company/users')
	else:
		context = {}
		
		if request.method == 'POST':
			if request.POST.get('organization') in [x.username for x in User.objects.all()]:
				messages.add_message(request, messages.WARNING, 'Username already exists!')
				return redirect('/company/signup')
			form = forms.CreateCompanyForm(request.POST)
			context['form'] = form
			d = {'username': request.POST.get('organization'),
				 'password1': request.POST.get('password1'),
				 'password2': request.POST.get('password2')}
			data = UserCreationForm(data=d)
			if data.is_valid():
				try:
					data.save()
					entry = models.Company(relation=User.objects.get(username=request.POST.get('organization')),
										   tokens=0,
										   lunch=0,
										   dayoff=0,
										   day_of_month=1,
										   payoff=0)
					
					entry.save()
					return redirect('/company/login')
				except:
					errormsg = 'Creation failed'
					context['errormsg'] = errormsg
		if 'form' not in context:
			form = forms.CreateCompanyForm()
			context['form'] = form

		return render(request, 'company/signup.html', context)
    

def companyLogin(request):
	if request.user.is_authenticated and request.user.username in [x.relation.username for x in models.Company.objects.all()]:
		return redirect('/company/users')
	else:
		context = {'form': forms.CompanyLogin}
		
		if request.method == 'POST':
			organization = request.POST.get('organization')
			password = request.POST.get('password')

			user = authenticate(request, username=organization, password=password)

			if user is not None:
				login(request, user)
				return redirect('/company/users')
			else:
				messages.add_message(request, messages.WARNING, 'Incorrect username or password')

		return render(request, 'company/login.html', context)


def companyLogout(request):
	logout(request)
	return redirect('/company/login')


@login_required(login_url='/company/login')
def companyUsers(request):

	company = request.user.username

	if company not in [x.relation.username for x in models.Company.objects.all()]:
		return redirect('/company/login')

	balance = models.Company.objects.get(relation__username=company).tokens
	form = forms.CreateStaffForm(my_choices=([(company, company),]))
	users = models.Staff.objects.filter(comp__relation__username=company)
	ctx = {'company': company, 'balance': balance, 'form': form, 'users': users}

	if request.method == 'POST':
		if request.POST.get('username') in [x.username for x in User.objects.all()]:
			messages.add_message(request, messages.WARNING, 'Username already exists!')
			return redirect('/company/users')
		form = forms.CreateStaffForm({'username': request.POST.get('username'),
									  'first_name': request.POST.get('first_name'),
									  'last_name': request.POST.get('last_name'),
									  'password1': request.POST.get('password1'), 
									  'password2': request.POST.get('password2'),
									  'tokens': request.POST.get('tokens')},
									   my_choices=([(company, company),]))
		ctx['form'] = form
		d = {'username': request.POST.get('username'),
			 'password1': request.POST.get('password1'),
			 'password2': request.POST.get('password2')}
		data = UserCreationForm(data=d)
		if data.is_valid():
			try:
				data.save()
				tokens = int(request.POST.get('tokens'))
				res = balance - tokens
				if res < 0:
					messages.add_message(request, messages.WARNING, 'Insufficient funds in your account!')
					tokens = 0
				entry = models.Staff(rel=User.objects.get(username=request.POST.get('username')),
									 first_name=request.POST.get('first_name'),
									 last_name=request.POST.get('last_name'),
									 comp=models.Company.objects.get(relation__username=request.user),
									 tokens=tokens)
				entry.save()

				models.Company.objects.filter(relation__username=company).update(tokens=res)
				return redirect('/company/users')
			except:
				errormsg = 'Creation failed'
				ctx['errormsg'] = errormsg

	return render(request, 'company/users.html', ctx)


@login_required(login_url='/company/login')
def companyDashboard(request):
	company = request.user.username

	if company not in [x.relation.username for x in models.Company.objects.all()]:
		return redirect('/company/login')

	balance = models.Company.objects.get(relation__username=company).tokens

	if request.method == 'POST':

		if request.POST.get('lunch') is not None:
			price = request.POST.get('lunch')
			models.Company.objects.filter(relation__username=company).update(lunch=price)

		if request.POST.get('dayoff') is not None:
			price = request.POST.get('dayoff')
			models.Company.objects.filter(relation__username=company).update(dayoff=price)

		if request.POST.get('tokens') is not None:
			tokens = int(request.POST.get('tokens'))
			total = int(models.Company.objects.get(relation__username=company).tokens) + int(request.POST.get('tokens'))
			models.Company.objects.filter(relation__username=company).update(tokens=total)

		if request.POST.get('day_of_month') is not None and request.POST.get('amount') is not None:
			day_of_month = request.POST.get('day_of_month')
			amount = request.POST.get('amount')
			models.Company.objects.filter(relation__username=company).update(day_of_month=day_of_month, payoff=amount)

		return redirect('/company/dashboard')


	priceForm = forms.ManagePricing(initial={'lunch': models.Company.objects.get(relation__username=company).lunch,
											 'dayoff': models.Company.objects.get(relation__username=company).dayoff,
												 })
	buyForm = forms.BuyTokens()
	scheduleForm = forms.SchedulePayoff(initial={'day_of_month': models.Company.objects.get(relation__username=company).day_of_month,
												 'amount': models.Company.objects.get(relation__username=company).payoff,
												 })
	ctx = {
			'company': company,
			'balance': balance,
			'priceForm': priceForm, 
			'buyForm': buyForm,
			'scheduleForm': scheduleForm,
		  }

	return render(request, 'company/dashboard.html', ctx)


@login_required(login_url='/company/login')
def companyUser(request, pk):

	company = request.user.username

	if company not in [x.relation.username for x in models.Company.objects.all()]:
		return redirect('/company/login')

	account = models.Company.objects.get(relation__username=company).tokens
	full_user = models.Staff.objects.get(rel__username=pk)
	ctx = {'company': company,
		   'account': account,
		   'full_user': full_user,
		   'username': full_user.rel.username,
		   'first_name': full_user.first_name,
		   'last_name': full_user.last_name,
		   'balance': full_user.tokens,
		   'form': forms.UpdateStaffForm()
	}

	if request.method == 'POST':

		if request.POST.get('tokens') is not None:
			total = int(models.Staff.objects.get(rel__username=pk).tokens) + int(request.POST.get('tokens'))
			balance = int(models.Company.objects.get(relation__username=company).tokens) - int(request.POST.get('tokens'))
			if balance >= 0:
				models.Company.objects.filter(relation__username=company).update(tokens=balance)
				models.Staff.objects.filter(rel__username=pk).update(tokens=total)
			else:
				messages.add_message(request, messages.WARNING, 'Insufficient funds in your account!')

			return redirect('/company/user/{}'.format(pk))

		elif request.POST.get('delete') is not None:
			full_user.delete()
			User.objects.get(username=pk).delete()

			return redirect('/company/users')

	return render(request,'company/user.html', ctx)


def userLogin(request):
	if request.user.is_authenticated:
		return redirect('/userpage/{}'.format(request.user.username))
	else:
		if request.method == 'POST':
			username = request.POST.get('username')
			password = request.POST.get('password')

			user = authenticate(request, username=username, password=password)

			if user is not None:
				login(request, user)
				return redirect('/userpage/{}'.format(username))
			else:
				messages.add_message(request, messages.WARNING, 'Incorrect username or password')

		context = {'form': forms.UserLogin()}
		return render(request, 'user/login.html', context)


def userLogout(request):
	logout(request)
	return redirect('/login')


@login_required(login_url='/login')
def userPage(request, pk):
	user = models.Staff.objects.get(rel__username=request.user.username)
	balance = user.tokens
	company = models.Company.objects.get(relation__username=user.comp.relation.username)
	currentLunch = company.lunch
	currentDayoff = company.dayoff

	if request.method =='POST':

		if request.POST.get('buy_lunch') is not None:
			rest = balance - currentLunch
			if rest < 0:
				messages.add_message(request, messages.WARNING, 'Insufficient funds in your account!')
			else:
				models.Staff.objects.filter(rel__username=user.rel.username).update(tokens=rest)
				messages.success(request, 'You bought a lunch')

		if request.POST.get('have_day_off') is not None:
			rest = balance - currentDayoff
			if rest < 0:
				messages.add_message(request, messages.WARNING, 'Insufficient funds in your account!')
			else:
				models.Staff.objects.filter(rel__username=user.rel.username).update(tokens=rest)
				messages.success(request, 'You scheduled a dayoff')

		if request.POST.get('withdraw') is not None:
			withdraw = int(request.POST.get('withdraw'))
			rest = balance - withdraw
			if rest < 0:
				messages.add_message(request, messages.WARNING, 'Insufficient funds in your account!')
			else:
				models.Staff.objects.filter(rel__username=user.rel.username).update(tokens=rest)
				messages.success(request, 'You have withdrawn {} tokens'.format(withdraw))

		if request.POST.get('username') is not None and request.POST.get('amount') is not None:
			sent = int(request.POST.get('amount'))
			rest = balance - sent
			if rest < 0:
				messages.add_message(request, messages.WARNING, 'Insufficient funds in your account!')
			elif request.POST.get('username') not in [x.rel.username for x in models.Staff.objects.filter(rel__username=request.POST.get('username'))]:
				messages.add_message(request, messages.WARNING, 'Such username does not exist')
			else:
				models.Staff.objects.filter(rel__username=user.rel.username).update(tokens=rest)
				upd = int(models.Staff.objects.get(rel__username=request.POST.get('username')).tokens) + sent
				models.Staff.objects.filter(rel__username=request.POST.get('username')).update(tokens=upd)
				messages.success(request, 'You have successfully sent {} tokens'.format(sent))

		return redirect('/userpage/{}'.format(pk))

	form1 = forms.WithdrawTokens()
	form2 = forms.SendTokens()
	ctx = {
            'user': user,
            'company': company,
            'balance': balance,
            'currentLunch': currentLunch,
            'currentDayoff': currentDayoff,
            'form1': form1,
            'form2': form2,
          }
	return render(request, 'user/userpage.html', ctx)






			
	    



