from . import models
import datetime

def payTokens():
	companies = models.Company.objects.all()

	for company in companies:
	# Determine the number of days in current month, shift payoff date if necessary
		if calendar.monthrange(int(datetime.datetime.now().strftime("%y")), int(datetime.datetime.now().strftime("%m")))[1] < int(company.day_of_month):
			payoff_date = calendar.monthrange(int(datetime.datetime.now().strftime("%y")), int(datetime.datetime.now().strftime("%m")))[1]
		else:
			payoff_date = int(company.day_of_month)

		if payoff_date == int(datetime.datetime.now().strftime("%d")):
			for employee in models.Staff.objects.filter(comp=company):
				rest = int(models.Company.objects.get(relation__username=company).tokens) - int(company.payoff)
				if rest > 0:
					balance = int(models.Staff.objects.get(rel__username=employee.rel.username).tokens) + int(company.payoff)
					models.Staff.objects.filter(rel__username=employee.rel.username).update(tokens=balance)
					models.Company.objects.filter(relation__username=company).update(tokens=rest)


#For test purposes
def payTokens1():
	companies = models.Company.objects.all()

	for company in companies:
		payoff_time = int(company.day_of_month)

		if payoff_time == int(datetime.datetime.now().strftime("%M")):
			for employee in models.Staff.objects.filter(comp=company):
				rest = int(models.Company.objects.get(relation__username=company).tokens) - int(company.payoff)
				if rest > 0:
					balance = int(models.Staff.objects.get(rel__username=employee.rel.username).tokens) + int(company.payoff)
					models.Staff.objects.filter(rel__username=employee.rel.username).update(tokens=balance)
					models.Company.objects.filter(relation__username=company).update(tokens=rest)
