from django.contrib.auth.models import User
from django.db import models

# Create your models here.

class Company(models.Model):

	relation = models.OneToOneField(User, on_delete=models.CASCADE, primary_key=True)
	tokens = models.PositiveIntegerField()
	lunch = models.PositiveIntegerField()
	dayoff = models.PositiveIntegerField()
	day_of_month = models.PositiveIntegerField()
	payoff = models.PositiveIntegerField()

	def __str__(self):
		return self.relation.username


class Staff(models.Model):

	rel = models.OneToOneField(User, on_delete=models.CASCADE, primary_key=True)
	first_name = models.CharField(max_length=30)
	last_name = models.CharField(max_length=30)
	comp = models.ForeignKey(Company, on_delete=models.CASCADE)
	tokens = models.PositiveIntegerField()

	def __str__(self):
		return self.first_name + ' ' + self.last_name



